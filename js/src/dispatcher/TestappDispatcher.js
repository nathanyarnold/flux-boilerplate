var AppDispatcher = require('../dispatcher/AppDispatcher');
var TestappStore     = require('../stores/TestappStore');
var TestappConstants = require('../constants/TestappConstants');

/**
 * Register callback to handle all updates
 */
AppDispatcher.register(function(action) {
    console.log('Dispatcher.Testapp callback("'+ action.actionType +'")');

    switch(action.actionType) {
        case TestappConstants.TESTAPP_SET_NUMBER:
            var number = action.number;
            TestappStore.setNumber(number);
            TestappStore.emitChange();
            break;
        case TestappConstants.TESTAPP_DECREASE_NUMBER:
            TestappStore.decreaseNumber();
            TestappStore.emitChange();
            break;
        case TestappConstants.TESTAPP_INCREASE_NUMBER:
            TestappStore.increaseNumber();
            TestappStore.emitChange();
            break;
    };
});

