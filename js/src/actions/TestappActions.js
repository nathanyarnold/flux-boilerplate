var AppDispatcher    = require('../dispatcher/AppDispatcher');
var TestappConstants = require('../constants/TestappConstants');
var TestappStore     = require('../stores/TestappStore');


var TestappActions = {
    __name__: 'Actions.Testapp',

    setNumber: function(number) {
        console.log(this.__name__ +'.setNumber('+ number +')');
        AppDispatcher.dispatch({
            "actionType": TestappConstants.TESTAPP_SET_NUMBER,
            "number": number
        });
    },

    decreaseNumber: function() {
        console.log(this.__name__ +'.decreaseNumber()');
        AppDispatcher.dispatch({
            "actionType": TestappConstants.TESTAPP_DECREASE_NUMBER
        });
    },

    increaseNumber: function() {
        console.log(this.__name__ +'.increaseNumber()');
        AppDispatcher.dispatch({
            "actionType": TestappConstants.TESTAPP_INCREASE_NUMBER
        });
    },

    EOF: null
};

module.exports = TestappActions;


// To access Flux Actions from outside of Node-generated environment, 
// we're going to put into a global namespace. For now. 
window.SiteName.Actions.TestappActions = TestappActions;



