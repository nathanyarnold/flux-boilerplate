var keyMirror = require('keymirror');

module.exports = keyMirror({
    TESTAPP_SET_NUMBER: null,
    TESTAPP_DECREASE_NUMBER: null,
    TESTAPP_INCREASE_NUMBER: null
});

