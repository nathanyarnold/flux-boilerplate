// Create Global Company Namespace (eg. JoeFresh)
if (typeof window.SiteName == "undefined") {
    window.SiteName = { 
        Apps:{}, 
        Actions:{}, 
        Data:{} 
    };
};


// browserify components
var $                  = require('jquery');
var React              = require('react');
var ReactDOM           = require('react-dom');
var TestappStore       = require('./stores/TestappStore');
var TestappControlsApp = require('./components/App.TestappControls.react');
var TestappDisplayApp  = require('./components/App.TestappDisplay.react');


// Put app in Global namespace, so you can initialize it from elsewhere, if you need to
SiteName.Apps = function() {
    // PRIVATE PROPERTIES
    __name__ = 'SiteName.Apps';

    // PRIVATE METHODS
    _initTestapp = function() {
        // check which view we're on
        $body = $(document.body);
        // if view is 'testapp'
        if ($body.hasClass('testapp')) {
            // init Store
            TestappStore.init();
            // init React Apps
            _initTestappControls();
            _initTestappDisplay();
        };
    };

    _initTestappControls = function() {
        var node = document.getElementById('testapp-controls');
        if (node) {
            ReactDOM.render(<TestappControlsApp />, node);
        };
    };

    _initTestappDisplay = function() {
        var node = document.getElementById('testapp-display');
        if (node) {
            ReactDOM.render(<TestappDisplayApp />, node);
        };
    };

    // PUBLIC METHODS
    return {
        init: function() {
            _initTestapp();
        },

        EOF: null
    };
}();

// Initialize App
SiteName.Apps.init();

