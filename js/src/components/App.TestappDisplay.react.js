var React = require('react');
var TestappStore = require('../stores/TestappStore');


// GET DATA FROM CART STORE
function getStoreData() {
    return {
        data: TestappStore.getData()
    };
};


// DEFINE REACT APP
var TestappDisplay = React.createClass({
    __name__: 'React.App.TestappDisplay',

    getInitialState: function() {
        console.log(this.__name__ +'.getInitialState()');
        return getStoreData();
    },

    componentDidMount: function() {
        console.log(this.__name__ +'.componentDidMount()');
        TestappStore.addChangeListener(this._onChange);
    },

    componentDidUpdate: function() {
        console.log(this.__name__ +'.componentDidUpdate()');
    },

    componentWillUnmount: function() {
        console.log(this.__name__ +'.componentWillUnmount()');
        TestappStore.removeChangeListener(this._onChange);
    },

    render: function() {
        console.log(this.__name__ +'.render()');
        var data = this.state.data;

        return (
            <span>{data.number}</span>
        );
    },

    // Event handler for 'change' events coming from store EVENT EMITTER.
    _onChange: function() {
        console.log(this.__name__ +'._onChange()');
        this.setState(getStoreData());
    },

    EOF: null
});

module.exports = TestappDisplay;

