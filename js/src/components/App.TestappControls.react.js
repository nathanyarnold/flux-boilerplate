var React = require('react');
var TestappStore = require('../stores/TestappStore');


// GET DATA FROM CART STORE
function getStoreData() {
    return {
        data: TestappStore.getData()
    };
};


// DEFINE REACT APP
var TestappControls = React.createClass({
    __name__: 'React.App.TestappControls',

    getInitialState: function() {
        console.log(this.__name__ +'.getInitialState()');
        return getStoreData();
    },

    componentDidMount: function() {
        console.log(this.__name__ +'.componentDidMount()');
        TestappStore.addChangeListener(this._onChange);
    },

    componentDidUpdate: function() {
        console.log(this.__name__ +'.componentDidUpdate()');
    },

    componentWillUnmount: function() {
        console.log(this.__name__ +'.componentWillUnmount()');
        TestappStore.removeChangeListener(this._onChange);
    },

    render: function() {
        console.log(this.__name__ +'.render()' );
        var data = this.state.data;

        return (
            <span>
                <input type="button" value="-" 
                    onClick={this._handleDecreaseNumber} />
                <input type="number" value={data.number} 
                    onFocus={this._handleFocus}
                    onChange={this._handleChangeNumber} />
                <input type="button" value="+" 
                    onClick={this._handleIncreaseNumber} />
            </span>
        );
    },

    // Event handler for 'change' events coming from store EVENT EMITTER.
    _onChange: function() {
        console.log(this.__name__ +'._onChange()');
        this.setState(getStoreData());
    },

    // Event handler for user-interaction
    _handleDecreaseNumber: function() {
        console.clear();
        console.log(this.__name__ +'._handleDecreaseNumber()');
        SiteName.Actions.TestappActions.decreaseNumber();
    },
    _handleIncreaseNumber: function() {
        console.clear();
        console.log(this.__name__ +'._handleIncreaseNumber()');
        SiteName.Actions.TestappActions.increaseNumber();
    },
    _handleChangeNumber: function(event) {
        console.clear();
        console.log(this.__name__ +'._handleChangeNumber()');
        var target = event.target;
        SiteName.Actions.TestappActions.setNumber(target.value);
    },

    _handleFocus: function(event) {
        console.clear();
        console.log(this.__name__ +'._handleFocus()');
        var target = event.target;
        target.select();
    },

    EOF: null
});

module.exports = TestappControls;

