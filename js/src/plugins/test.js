// Test jQuery Plugin
(function() {
    // get jQuery
    var $ = require('jquery');

    // properties
    var pluginName = 'testPlugin',
        selector   = '#jquery-app';

    // methods
    var methods = {
            init: function($root) {
                console.log(pluginName +'.init()');

                // add click eventListener
                $root.on('click', methods.handleClick);
            },

            handleClick: function(ev) {
                console.clear();
                console.log(pluginName +'.handleClick()');
                // ask user for a number
                var value = window.prompt('Give me a number!');
                // update store with action
                SiteName.Actions.TestappActions.setNumber( parseInt(value) );
            },

            EOF: null
        };

    // controller
    $.fn[pluginName] = function(method) {
        methods.init(this);
    };

    // auto-init yourself
    $(selector)[ pluginName ]('init');
})();

