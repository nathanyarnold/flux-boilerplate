var $ = require('jquery');
var $cookie = require('jquery.cookie');

var AppDispatcher = require('../dispatcher/AppDispatcher');
var EventEmitter  = require('events').EventEmitter;
var assign        = require('object-assign');

var TestAppConstants = require('../constants/TestappConstants');

// PRIVATE PROPERTIES
var CHANGE_EVENT = 'change';
var __name__ = 'Store.Testapp';
var _cookieKey = 'TestappNumber';

// MODEL
var _data = {
    number: 0,
};

// PRIVATE METHODS
function _init() {
    console.log(__name__ +'._init()');

    // get Cookie
    var cartNumber = $.cookie(_cookieKey);
    console.log(' - cookie is currently set: '+ cartNumber);
    if (cartNumber) {
        _setNumber(cartNumber);
    };
};

function _setNumber(number) {
    console.log(__name__ +'._setNumber('+ number +')');

    // set data
    _data.number = parseInt(number);

    // set cookie
    $.cookie(_cookieKey, _data.number);

    return _data.number;
};


// EVENT EMITTER
var TestappStore = assign({}, EventEmitter.prototype, {

    // PUBLIC METHODS
    init: function() {
        console.log(__name__ +'.init()');
        _init();
    },

    getData: function() {
        console.log(__name__ +'.getData()');
        return _data;
    },
    setNumber: function(number) {
        console.log(__name__ +'.setNumber('+ number +')');
        return _setNumber(number);
    },
    decreaseNumber: function() {
        console.log(__name__ +'.decreaseNumber()');
        return _setNumber(_data.number - 1);
    },
    increaseNumber: function() {
        console.log(__name__ +'.increaseNumber()');
        return _setNumber(_data.number + 1);
    },

    // EVENT METHODS
    emitChange: function() {
        console.log(__name__ +'.emitChange('+ CHANGE_EVENT +')');
        this.emit(CHANGE_EVENT);
    },
    addChangeListener: function(callback) {
        console.log(__name__ +'.addChangeListener()');
        this.on(CHANGE_EVENT, callback);
    },
    removeChangeListener: function(callback) {
        console.log(__name__ +'.removeChangeListener()');
        this.removeListener(CHANGE_EVENT, callback);
    },

    EOF: null
});


/**
 * Export Module
 */
module.exports = TestappStore;

