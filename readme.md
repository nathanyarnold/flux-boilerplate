# Flux/React - boilerplate

A boilerplate Flux/React setup, with one generic store, a couple generic components, one external (jQuery) plugin, and all wired up using Node, Grunt & Browserify. 

Note: I've used 'SiteName' as a generic namespace for this app. You can change this to whatever is unique to your project. This was done largely, so that Flux Actions could be exposed to any external plugin/application via that namespace.

## Requirements

1. Node/NPM
2. Grunt CLI

## Installation

Fork or clone the repository locally. Navigate to that folder in your terminal and run the following command: 

* `npm install`

## Flux/React Development

We're using Browserify to compile the app (and all its dependancies) into one common JS file. To run this, use the following Grunt command:

* `grunt build:dev`

This will place the dev build file in the following location:

* `js/build/dev.js`

You can also have 'grunt watch' compile it every time a JS file in the app is saved:

* `grunt watch` or `grunt watch:browserify`

## Flux/React Production

When the Flux/React App is ready to go live, run the following Grunt command:

* `grunt build:prod`

This will create the uglified build file named with the version number in your Grunt config file. The build file will be placed in the following location:

* `js/build/prod.js`

## CSS Development

The CSS in this app is pre-processed and compiled using LESS. To do this, run the following Grunt command:

* `grunt less`

You can also have 'grunt watch' compile it every time a LESS file is saved. This is the same command that we use for building the Flux/React App:

* `grunt watch` or `grunt watch:less`

## Some notes on Flux/React development

All flux/js react files are organized into the following directories:

* `js/`
    * `src/`
        * `_app.js`      <- file that initializes the whole flux architecture
        * `actions/`     <- Flux Actions
        * `components/`  <- React components, for rendering DOM and listening to events
        * `constants/`   <- Just some constants that we'll use all over
        * `dispatcher/`  <- Flux Dispatcher
        * `plugins/`     <- Keep other JS plugins here (jQuery?)
        * `stores/`      <- Flux Stores - basically ALL the business logic
    * `build/`
        * `dev.js`       <- For dev use only. Keeps console statements, formatting, naming.
        * `prod.js`      <- For prod use only. Minified and uglified.

