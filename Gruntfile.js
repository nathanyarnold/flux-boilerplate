module.exports = function(grunt) {

    // Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        meta: {
            flux_version: '0.0.1'
        },
        uglify: {
            options: {
                banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n'
            },
            build: {
                src:  'js/build/dev.js',
                dest: 'js/build/prod.js'
            }
        },
        less: {
            development: {
                options: {
                    env: 'development',
                    dumpLineNumbers: 'comments',
                    ieCompat: true
                },
                files: {
                    'css/build/dev.css': 'css/src/styles.less'
                }
            },
            production: {
                options: {
                    env: 'production',
                    dumpLineNumbers: '',
                    ieCompat: true
                },
                files: {
                    'css/build/prod.css': 'css/src/styles.less'
                }
            },
        },
        browserify: {
            dist: {
                files: {
                    'js/build/dev.js': ['js/src/**/*.js']
                }
            }
        },
        watch: {
            // Run 'grunt watch:less' to build LESS files when changes are detected
            less: {
                files: 'css/src/**/*.less',
                tasks: ['less:development']
            },
            // Run 'grunt watch:browserify' to build JavaScript files when changes are detected
            browserify: {
                files: 'js/src/**/*.js',
                tasks: ['browserify']
            }
        }
    });

    // Load plugins
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-browserify');

    // Tasks
    grunt.registerTask('default',    ['less:development', 'browserify']);
    grunt.registerTask('build:dev',  ['less:development', 'browserify']);
    grunt.registerTask('build:prod', ['less:production',  'browserify', 'uglify']);
    grunt.registerTask('build:css',  ['less:development']);

};

